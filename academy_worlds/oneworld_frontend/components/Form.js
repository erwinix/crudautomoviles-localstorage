import React from "react";
import { Container, Row, Col } from "reactstrap";

const Form = () => {
  return (
    <section className="section bg-light" id="about">
      <Container>
        <Row className="justify-content-center">
          <Col lg={6} md={8}>
            <div className="title text-center mb-5">
              <h3 className="font-weight-normal text-dark">Form of Contact</h3>
            </div>
          </Col>
        </Row>
        <Row>
          <form className='formContact'>
            <formGroup className=''>
              <label htmlFor="name">Name</label>
              <input type="text" name="name" className='form-control' />
            </formGroup>
            <formGroup className='{styles.inputGroup}'>
              <label htmlFor="email">Email</label>
              <input type="email" name="email" className='form-control' />
            </formGroup>
            <formGroup className='{styles.inputGroup}'>
              <label htmlFor="message">Message</label>
              <input type="text" name="message" className='form-control' />
            </formGroup>
            <input className="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit" />
          </form>
        </Row>
      </Container>
    </section>
  );
};
export default Form;
