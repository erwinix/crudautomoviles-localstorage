
import Layout from "../components/Layout";
import Header from "../components/Header";
import Hero from "../components/Hero";
import Feature from '../components/Feature';
import Service from '../components/Service';
import About from '../components/About';
import Footer from '../components/Footer';
import Form from '../components/Form';
const Index = () => {
  return (
    <Layout pageTitle="One World Language Academy">
      <Header />
      <Hero />
      <Feature/>
      <Service />
      <About />
      <Form />
      <Footer />
    </Layout>
  )
}
export default Index;